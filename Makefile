CPP = g++
CPPFLAGS = -Wall -I.
LIBS = -lpthread

# OBJECTS =
# TARGET =

# $^ - objects
# $@ - target

all: dir_struct ./bin/hv_status message

./bin/hv_status: ./lib/source.o ./lib/easywsclient.o
	$(CPP) $(CPPFLAGS) $^ -o $@ $(LIBS)

./lib/%.o: ./src/%.cpp
	$(CPP) $(CPPFLAGS) -c $^ -o $@ $(LIBS)

run:
	@sudo ./bin/hv_status

dir_struct:
	@mkdir -p bin lib

message:
	@echo '#########################################'
	@echo '############### COMPLETED ###############'
	@echo '#########################################'

clean:
	@rm -f ./lib/* ./bin/*
